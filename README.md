# IP Blacklist 🚫🌐

## Description du Projet 📘

Le projet IP Blacklist vise à fournir des listes régulièrement mises à jour d'adresses IP associées à des activités malveillantes, des nœuds TOR, et des proxys peu sûrs. Ces listes sont destinées à aider les administrateurs réseaux et les professionnels de la cybersécurité à bloquer ou à surveiller ces adresses dans leurs systèmes.


## Fichiers 📁

Les listes d'IP sont enregistrées dans des fichiers texte :
- `MALWARES.txt` : Adresses liées à des malwares et C2.
- `TOR.txt` : Adresses IP de sorties TOR.
- `BAD_PROXIES.txt` : Proxys connus pour être utilisés à des fins malveillantes.

## Utilisation 🖥️

Pour utiliser ces listes :
1. Téléchargez le fichier correspondant à vos besoins.
2. Intégrez le fichier dans vos outils de surveillance ou de blocage réseau.

## Contribution 🤝

Les contributions pour améliorer les scripts de récupération ou proposer de nouvelles sources d'IP sont toujours les bienvenues. Pour contribuer, veuillez soumettre une pull request avec vos modifications ou améliorations.

## Licence 📜

Ce projet est distribué sous la licence MIT, permettant une utilisation libre et ouverte dans des projets personnels et professionnels.
